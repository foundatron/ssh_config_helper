package configs

import (
	"encoding/json"
	"io/ioutil"
)

//Config with stuff
type Config map[string]interface{}

//LoadConfig json config file
func LoadConfig(path string) (Config, error) {
	var m map[string]interface{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return m, err
	}

	err = json.Unmarshal(data, &m)
	return m, err
}
