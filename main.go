package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/foundatron/ssh_config_helper/configs"
)

func getExternalAddress() string {
	resp, err := http.Get("http://whatismyip.akamai.com")
	if err != nil {
		log.Fatal(err)
	}
	externalAddress, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return string(externalAddress)
}

func main() {
	address1 := getExternalAddress()
	fmt.Printf("External Address is: %v", address1)
	c, err := configs.LoadConfig("config.json")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Config: %v", c)
}
